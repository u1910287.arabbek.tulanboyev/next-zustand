import React, { useState } from "react";
import styles from "./style.module.scss";
import useStore from "../../../store/store.ts";
export function Counter() {
  const store = useStore();
  const [incrementAmount, setIncrementAmount] = useState("2");

  return (
    <div>
      <div className={styles.row}>
        <button
          className={styles.button}
          aria-label="Increment value"
          onClick={() => store.increment()}
        >
          +
        </button>
        <span className={styles.value}>{store.count}</span>
        <button
          className={styles.button}
          aria-label="Decrement value"
          onClick={() => store.decrement()}
        >
          -
        </button>
      </div>
      <div className={styles.row}>
        <input
          className={styles.textbox}
          aria-label="Set increment amount"
          value={incrementAmount}
          onChange={(e) => setIncrementAmount(e.target.value)}
        />
        <button
          className={styles.button}
          onClick={() => store.incrementByAmount(Number(incrementAmount) || 0)}
        >
          Add Amount
        </button>
        <button
          className={styles.button}
          onClick={() => store.decrementByAmount(Number(incrementAmount) || 0)}
        >
          Remove amount
        </button>
      </div>
    </div>
  );
}
