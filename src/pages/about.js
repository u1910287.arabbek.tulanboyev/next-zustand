import { About } from '@/components/Pages/About'
import SEO from '@/components/SEO'

export default function AboutUs() {
  return (
    <>
      <SEO />
      <About />
    </>
  )
}
